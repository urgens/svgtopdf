#!/bin/bash
set -e

cd /zip
for file in *.zip; do
    unzip $file -d /UnzippedDirectory
    filebasename="$(basename $file)"
    filename="${filebasename%%.*}"
    echo $filename
    cd /UnzippedDirectory
    find /UnzippedDirectory -type f -name "*.svg" -exec bash -c 'echo $0 && rsvg-convert -f pdf -o /UnzippedDirectory/$(basename $0).pdf "$0"' {} \;
    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOutputFile=$filename.pdf `ls -1v *.pdf`
    cp /UnzippedDirectory/$filename.pdf /zip/$filename.pdf
    cd /zip
    rm -rf /UnzippedDirectory
done
