FROM alpine:latest
ENV PYTHONUNBUFFERED 1

RUN apk --no-cache add bash ttf-dejavu librsvg ghostscript unzip \
  && mkdir /zip
COPY convert.sh /usr/local/bin/
RUN ln -s usr/local/bin/docker-entrypoint.sh /
# RUN mkdir /UnzippedDirectory
WORKDIR /usr/local/bin/
RUN chmod +x /usr/local/bin/convert.sh
ENTRYPOINT ["bash", "/usr/local/bin/convert.sh"]
